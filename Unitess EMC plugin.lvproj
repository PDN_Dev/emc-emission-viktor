﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="15008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Lib" Type="Folder" URL="../Lib">
			<Property Name="NI.DISK" Type="Bool">true</Property>
		</Item>
		<Item Name="UniLavLIB_ControlsUI" Type="Folder">
			<Item Name="1_CUI_Example.vi" Type="VI" URL="/UnitessLIB/UniLavLIB_ControlsUI/!Builds/UniLavLIB_ControlsUI.llb/1_CUI_Example.vi"/>
			<Item Name="Button.ctl" Type="VI" URL="/UnitessLIB/UniLavLIB_ControlsUI/!Builds/UniLavLIB_ControlsUI.llb/Button.ctl"/>
			<Item Name="CUI Action Open FP.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_ControlsUI/!Builds/UniLavLIB_ControlsUI.llb/CUI Action Open FP.vi"/>
			<Item Name="CUI Action.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_ControlsUI/!Builds/UniLavLIB_ControlsUI.llb/CUI Action.vi"/>
			<Item Name="CUI Control sett.ctl" Type="VI" URL="../../UnitessLIB/UniLavLIB_ControlsUI/!Builds/UniLavLIB_ControlsUI.llb/CUI Control sett.ctl"/>
			<Item Name="CUI Controls sett.ctl" Type="VI" URL="../../UnitessLIB/UniLavLIB_ControlsUI/!Builds/UniLavLIB_ControlsUI.llb/CUI Controls sett.ctl"/>
			<Item Name="CUI Extract settings from desc.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_ControlsUI/!Builds/UniLavLIB_ControlsUI.llb/CUI Extract settings from desc.vi"/>
			<Item Name="CUI GUI Silver.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_ControlsUI/!Builds/UniLavLIB_ControlsUI.llb/CUI GUI Silver.vi"/>
			<Item Name="CUI GUI Unitess Black.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_ControlsUI/!Builds/UniLavLIB_ControlsUI.llb/CUI GUI Unitess Black.vi"/>
			<Item Name="CUI GUI Unitess Blue steel.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_ControlsUI/!Builds/UniLavLIB_ControlsUI.llb/CUI GUI Unitess Blue steel.vi"/>
			<Item Name="CUI UI Style.ctl" Type="VI" URL="../../UnitessLIB/UniLavLIB_ControlsUI/!Builds/UniLavLIB_ControlsUI.llb/CUI UI Style.ctl"/>
			<Item Name="String.ctl" Type="VI" URL="/UnitessLIB/UniLavLIB_ControlsUI/!Builds/UniLavLIB_ControlsUI.llb/String.ctl"/>
		</Item>
		<Item Name="UniLavLIB_DriversAPI" Type="Folder">
			<Item Name="1 UDriver Example.vi" Type="VI" URL="/UnitessLIB/UniLavLIB_DriversAPI/!Builds/UniLavLIB_DriversAPI.llb/1 UDriver Example.vi"/>
			<Item Name="UDriver API.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_DriversAPI/!Builds/UniLavLIB_DriversAPI.llb/UDriver API.vi"/>
			<Item Name="UDriver Get Driver list.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_DriversAPI/!Builds/UniLavLIB_DriversAPI.llb/UDriver Get Driver list.vi"/>
		</Item>
		<Item Name="UniLavLIB_Error_Handler" Type="Folder">
			<Item Name="1 UError Example.vi" Type="VI" URL="/UnitessLIB/UniLavLIB_Error_Handler/1Builds/UniLavLIB_Error_Handler.llb/1 UError Example.vi"/>
			<Item Name="UError apply custom error code.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_Error_Handler/1Builds/UniLavLIB_Error_Handler.llb/UError apply custom error code.vi"/>
			<Item Name="UError clear ignored code.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_Error_Handler/1Builds/UniLavLIB_Error_Handler.llb/UError clear ignored code.vi"/>
			<Item Name="UError create delimeters.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_Error_Handler/1Builds/UniLavLIB_Error_Handler.llb/UError create delimeters.vi"/>
			<Item Name="UError format message.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_Error_Handler/1Builds/UniLavLIB_Error_Handler.llb/UError format message.vi"/>
			<Item Name="UError init.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_Error_Handler/1Builds/UniLavLIB_Error_Handler.llb/UError init.vi"/>
			<Item Name="UError Load Custom Error File.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_Error_Handler/1Builds/UniLavLIB_Error_Handler.llb/UError Load Custom Error File.vi"/>
			<Item Name="UError Save 2 file log.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_Error_Handler/1Builds/UniLavLIB_Error_Handler.llb/UError Save 2 file log.vi"/>
			<Item Name="UError.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_Error_Handler/1Builds/UniLavLIB_Error_Handler.llb/UError.vi"/>
		</Item>
		<Item Name="UniLavLIB_Menu" Type="Folder">
			<Item Name="1 UMenu Example.vi" Type="VI" URL="/UnitessLIB/UniLavLIB_Menu/1Build/UniLavLIB_Menu.llb/1 UMenu Example.vi"/>
			<Item Name="UMenu Back.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_Menu/1Build/UniLavLIB_Menu.llb/UMenu Back.vi"/>
			<Item Name="UMenu Engineeriong menu password.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_Menu/1Build/UniLavLIB_Menu.llb/UMenu Engineeriong menu password.vi"/>
			<Item Name="UMenu Init.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_Menu/1Build/UniLavLIB_Menu.llb/UMenu Init.vi"/>
			<Item Name="UMenu Serf.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_Menu/1Build/UniLavLIB_Menu.llb/UMenu Serf.vi"/>
			<Item Name="UMenu Settings.ctl" Type="VI" URL="../../UnitessLIB/UniLavLIB_Menu/1Build/UniLavLIB_Menu.llb/UMenu Settings.ctl"/>
			<Item Name="UMenu tab ref.ctl" Type="VI" URL="../../UnitessLIB/UniLavLIB_Menu/1Build/UniLavLIB_Menu.llb/UMenu tab ref.ctl"/>
		</Item>
		<Item Name="UniLavLIB_Useful" Type="Folder">
			<Item Name="Convert Panel to Screen Coordinate.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/Convert Panel to Screen Coordinate.vi"/>
			<Item Name="Create folder if not exist.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/Create folder if not exist.vi"/>
			<Item Name="CSV load.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/CSV load.vi"/>
			<Item Name="Excel export ArrayOfstr.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/Excel export ArrayOfstr.vi"/>
			<Item Name="Excel open.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/Excel open.vi"/>
			<Item Name="File Dialog.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/File Dialog.vi"/>
			<Item Name="Get All Controls Ref From VI.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/Get All Controls Ref From VI.vi"/>
			<Item Name="Get All inserted Controls Ref From Control.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/Get All inserted Controls Ref From Control.vi"/>
			<Item Name="Get EXE name.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/Get EXE name.vi"/>
			<Item Name="Get Exe Ver.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/Get Exe Ver.vi"/>
			<Item Name="Get monitor resolution.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/Get monitor resolution.vi"/>
			<Item Name="INI_SaveLoad.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/INI_SaveLoad.vi"/>
			<Item Name="IS_EXE.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/IS_EXE.vi"/>
			<Item Name="Make Top VI Frontmost.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/Make Top VI Frontmost.vi"/>
			<Item Name="Make VI Frontmost.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/Make VI Frontmost.vi"/>
			<Item Name="MCL or Tbl 2 Excel.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/MCL or Tbl 2 Excel.vi"/>
			<Item Name="Postfix2expV2_point.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/Postfix2expV2_point.vi"/>
			<Item Name="Postfix_ENG_2_RUS.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/Postfix_ENG_2_RUS.vi"/>
			<Item Name="RemoveBadSymbols (SubVI).vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/RemoveBadSymbols (SubVI).vi"/>
			<Item Name="RunDifferentFile.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/RunDifferentFile.vi"/>
			<Item Name="RunVI.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/RunVI.vi"/>
			<Item Name="SaveFileAsSTR.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/SaveFileAsSTR.vi"/>
			<Item Name="Set VI screen coordinates.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/Set VI screen coordinates.vi"/>
			<Item Name="Set Window Z-Position (hwnd).vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/Set Window Z-Position (hwnd).vi"/>
			<Item Name="Startup Add EXE to it.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/Startup Add EXE to it.vi"/>
			<Item Name="Startup Delete EXE from it.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/Startup Delete EXE from it.vi"/>
			<Item Name="Startup Is EXE in it.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/Startup Is EXE in it.vi"/>
			<Item Name="StopOrExit (SubVI).vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/StopOrExit (SubVI).vi"/>
			<Item Name="STR_translit.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/STR_translit.vi"/>
			<Item Name="Unitess_Format_Value.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/Unitess_Format_Value.vi"/>
			<Item Name="Variable_SaveLoad.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_Useful/!Builds/UniLavLIB_Useful.llb/Variable_SaveLoad.vi"/>
		</Item>
		<Item Name="UnitessPluginLink" Type="Folder">
			<Item Name="1 UPL Example plugin.vi" Type="VI" URL="/UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/1 UPL Example plugin.vi"/>
			<Item Name="2 UPL Example Control.vi" Type="VI" URL="/UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/2 UPL Example Control.vi"/>
			<Item Name="3 UPL Server Init.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/3 UPL Server Init.vi"/>
			<Item Name="4 UPL Server Cntr Regist.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/4 UPL Server Cntr Regist.vi"/>
			<Item Name="5 UPL Server Start.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/5 UPL Server Start.vi"/>
			<Item Name="6 UPL Server Event DONE.vi" Type="VI" URL="/UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/6 UPL Server Event DONE.vi"/>
			<Item Name="7 UPL Server STOP.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/7 UPL Server STOP.vi"/>
			<Item Name="Client Connection.ctl" Type="VI" URL="/UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/Client Connection.ctl"/>
			<Item Name="Server Connections.ctl" Type="VI" URL="../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/Server Connections.ctl"/>
			<Item Name="UPL Client Connect.vi" Type="VI" URL="/UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Client Connect.vi"/>
			<Item Name="UPL Client Disconnect.vi" Type="VI" URL="/UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Client Disconnect.vi"/>
			<Item Name="UPL Client Get Control.vi" Type="VI" URL="/UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Client Get Control.vi"/>
			<Item Name="UPL Client Set Control.vi" Type="VI" URL="/UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Client Set Control.vi"/>
			<Item Name="UPL Client Sys commands.vi" Type="VI" URL="/UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Client Sys commands.vi"/>
			<Item Name="UPL find control index.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL find control index.vi"/>
			<Item Name="UPL Get Exe Ver.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Get Exe Ver.vi"/>
			<Item Name="UPL Message Get Parsing.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Message Get Parsing.vi"/>
			<Item Name="UPL Message Set Parsing.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Message Set Parsing.vi"/>
			<Item Name="UPL Server Array pars Boolean.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server Array pars Boolean.vi"/>
			<Item Name="UPL Server Array pars Digital.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server Array pars Digital.vi"/>
			<Item Name="UPL Server Array pars String.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server Array pars String.vi"/>
			<Item Name="UPL Server Avalible Item.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server Avalible Item.vi"/>
			<Item Name="UPL Server Cntr Check if already exist.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server Cntr Check if already exist.vi"/>
			<Item Name="UPL Server Cntr Regist.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server Cntr Regist.vi"/>
			<Item Name="UPL Server Core.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server Core.vi"/>
			<Item Name="UPL Server create help Arguments.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server create help Arguments.vi"/>
			<Item Name="UPL Server create help.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server create help.vi"/>
			<Item Name="UPL Server Find Cntr Ref.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server Find Cntr Ref.vi"/>
			<Item Name="UPL Server Pars Array.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server Pars Array.vi"/>
			<Item Name="UPL Server Pars BOLLEAN.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server Pars BOLLEAN.vi"/>
			<Item Name="UPL Server Pars ComboBox.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server Pars ComboBox.vi"/>
			<Item Name="UPL Server Pars Delete Quotes.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server Pars Delete Quotes.vi"/>
			<Item Name="UPL Server Pars DIGITAL.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server Pars DIGITAL.vi"/>
			<Item Name="UPL Server Pars ENUM.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server Pars ENUM.vi"/>
			<Item Name="UPL Server Pars Message.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server Pars Message.vi"/>
			<Item Name="UPL Server Pars MulticolumnListBox.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server Pars MulticolumnListBox.vi"/>
			<Item Name="UPL Server Pars RadioButton.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server Pars RadioButton.vi"/>
			<Item Name="UPL Server Pars RING.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server Pars RING.vi"/>
			<Item Name="UPL Server Pars SLIDE id21.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server Pars SLIDE id21.vi"/>
			<Item Name="UPL Server Pars TabControl.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server Pars TabControl.vi"/>
			<Item Name="UPL Server Pars TIMESTAMP.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server Pars TIMESTAMP.vi"/>
			<Item Name="UPL Server Select Item.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server Select Item.vi"/>
			<Item Name="UPL Server value 2 str array.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server value 2 str array.vi"/>
			<Item Name="UPL Server value 2 str combox.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server value 2 str combox.vi"/>
			<Item Name="UPL Server value 2 str digit.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server value 2 str digit.vi"/>
			<Item Name="UPL Server value 2 str enum.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server value 2 str enum.vi"/>
			<Item Name="UPL Server value 2 str mclb.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server value 2 str mclb.vi"/>
			<Item Name="UPL Server value 2 str ring.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server value 2 str ring.vi"/>
			<Item Name="UPL Server value 2 str tab.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server value 2 str tab.vi"/>
			<Item Name="UPL Server VI Control Get.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server VI Control Get.vi"/>
			<Item Name="UPL Server VI Control Set.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server VI Control Set.vi"/>
			<Item Name="UPL Server VI Sys comand.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL Server VI Sys comand.vi"/>
			<Item Name="UPL TCP Multiple Connections Data.ctl" Type="VI" URL="/UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL TCP Multiple Connections Data.ctl"/>
			<Item Name="UPL TCP read Error.vi" Type="VI" URL="/UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL TCP read Error.vi"/>
			<Item Name="UPL TCP read Event Flag.vi" Type="VI" URL="/UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL TCP read Event Flag.vi"/>
			<Item Name="UPL TCP read Wait Event Done.vi" Type="VI" URL="/UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL TCP read Wait Event Done.vi"/>
			<Item Name="UPL TCP read.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL TCP read.vi"/>
			<Item Name="UPL TCP Write Error.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL TCP Write Error.vi"/>
			<Item Name="UPL TCP Write Event Done.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL TCP Write Event Done.vi"/>
			<Item Name="UPL TCP Write Event Flag.vi" Type="VI" URL="/UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL TCP Write Event Flag.vi"/>
			<Item Name="UPL TCP write.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL TCP write.vi"/>
			<Item Name="UPL_Cntr Regist Info.ctl" Type="VI" URL="../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL_Cntr Regist Info.ctl"/>
			<Item Name="UPL_Control.ctl" Type="VI" URL="../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL_Control.ctl"/>
			<Item Name="UPL_Controls.ctl" Type="VI" URL="../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL_Controls.ctl"/>
			<Item Name="upl_core_stm.ctl" Type="VI" URL="../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/upl_core_stm.ctl"/>
			<Item Name="UPL_delimeter_data.vi" Type="VI" URL="/UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL_delimeter_data.vi"/>
			<Item Name="UPL_Events_Type.ctl" Type="VI" URL="../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL_Events_Type.ctl"/>
			<Item Name="UPL_Exp2_Postfix.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL_Exp2_Postfix.vi"/>
			<Item Name="UPL_Postfix Parser.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL_Postfix Parser.vi"/>
			<Item Name="UPL_RunFile.vi" Type="VI" URL="../../UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL_RunFile.vi"/>
			<Item Name="UPL_StateMachine.ctl" Type="VI" URL="/UnitessLIB/UniLavLIB_PluginLink/!BUILD/UnitessPluginLink.llb/UPL_StateMachine.ctl"/>
		</Item>
		<Item Name="Unitess EMC plugin.vi" Type="VI" URL="../Unitess EMC plugin.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="user.lib" Type="Folder">
				<Item Name="Build Error Cluster__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/error/error.llb/Build Error Cluster__ogtk.vi"/>
				<Item Name="Current VIs Parents Ref__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/appcontrol/appcontrol.llb/Current VIs Parents Ref__ogtk.vi"/>
				<Item Name="Get Header from TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Header from TD__ogtk.vi"/>
				<Item Name="Get PString__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get PString__ogtk.vi"/>
				<Item Name="Get Strings from Enum TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Strings from Enum TD__ogtk.vi"/>
				<Item Name="Get Strings from Enum__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Strings from Enum__ogtk.vi"/>
				<Item Name="Set Enum String Value__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Set Enum String Value__ogtk.vi"/>
				<Item Name="Type Descriptor Enumeration__ogtk.ctl" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor Enumeration__ogtk.ctl"/>
				<Item Name="Type Descriptor Header__ogtk.ctl" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor Header__ogtk.ctl"/>
				<Item Name="Type Descriptor__ogtk.ctl" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor__ogtk.ctl"/>
				<Item Name="Variant to Header Info__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Variant to Header Info__ogtk.vi"/>
			</Item>
			<Item Name="vi.lib" Type="Folder">
				<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="Bit-array To Byte-array.vi" Type="VI" URL="/&lt;vilib&gt;/picture/pictutil.llb/Bit-array To Byte-array.vi"/>
				<Item Name="Build Exp Wvfrm (Wvfrm).vi" Type="VI" URL="/&lt;vilib&gt;/express/express shared/transition.llb/Build Exp Wvfrm (Wvfrm).vi"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Built App File Layout.vi" Type="VI" URL="/&lt;vilib&gt;/AppBuilder/Built App File Layout.vi"/>
				<Item Name="Calc Long Word Padded Width.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Calc Long Word Padded Width.vi"/>
				<Item Name="Check Color Table Size.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check Color Table Size.vi"/>
				<Item Name="Check Data Size.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check Data Size.vi"/>
				<Item Name="Check File Permissions.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check File Permissions.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Check Path.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check Path.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Close Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Close Registry Key.vi"/>
				<Item Name="Color to RGB.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/colorconv.llb/Color to RGB.vi"/>
				<Item Name="compatCalcOffset.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatCalcOffset.vi"/>
				<Item Name="compatFileDialog.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatFileDialog.vi"/>
				<Item Name="compatOpenFileOperation.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatOpenFileOperation.vi"/>
				<Item Name="compatOverwrite.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatOverwrite.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Create ActiveX Event Queue.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/Create ActiveX Event Queue.vi"/>
				<Item Name="Create Error Clust.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/Create Error Clust.vi"/>
				<Item Name="Create Mask By Alpha.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Create Mask By Alpha.vi"/>
				<Item Name="Delete Registry Value.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Delete Registry Value.vi"/>
				<Item Name="Destroy ActiveX Event Queue.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/Destroy ActiveX Event Queue.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="Dflt Data Dir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Dflt Data Dir.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Directory of Top Level VI.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Directory of Top Level VI.vi"/>
				<Item Name="Dynamic To Waveform Array.vi" Type="VI" URL="/&lt;vilib&gt;/express/express shared/transition.llb/Dynamic To Waveform Array.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="Escape Characters for HTTP.vi" Type="VI" URL="/&lt;vilib&gt;/printing/PathToURL.llb/Escape Characters for HTTP.vi"/>
				<Item Name="EventData.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/EventData.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="ex_CorrectErrorChain.vi" Type="VI" URL="/&lt;vilib&gt;/express/express shared/ex_CorrectErrorChain.vi"/>
				<Item Name="ex_ExpandPathIfRelative.vi" Type="VI" URL="/&lt;vilib&gt;/express/express shared/ExFile.llb/ex_ExpandPathIfRelative.vi"/>
				<Item Name="ex_GetAllExpressAttribs.vi" Type="VI" URL="/&lt;vilib&gt;/express/express shared/transition.llb/ex_GetAllExpressAttribs.vi"/>
				<Item Name="ex_SetAllExpressAttribs.vi" Type="VI" URL="/&lt;vilib&gt;/express/express shared/transition.llb/ex_SetAllExpressAttribs.vi"/>
				<Item Name="ex_WaveformAttribs.ctl" Type="VI" URL="/&lt;vilib&gt;/express/express shared/transition.llb/ex_WaveformAttribs.ctl"/>
				<Item Name="Express Waveform Components.vi" Type="VI" URL="/&lt;vilib&gt;/express/express shared/transition.llb/Express Waveform Components.vi"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="FindCloseTagByName.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindCloseTagByName.vi"/>
				<Item Name="FindElement.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindElement.vi"/>
				<Item Name="FindElementStartByName.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindElementStartByName.vi"/>
				<Item Name="FindEmptyElement.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindEmptyElement.vi"/>
				<Item Name="FindFirstTag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindFirstTag.vi"/>
				<Item Name="FindMatchingCloseTag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindMatchingCloseTag.vi"/>
				<Item Name="Flip and Pad for Picture Control.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Flip and Pad for Picture Control.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Generate Temporary File Path.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Generate Temporary File Path.vi"/>
				<Item Name="Get File Extension.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Get File Extension.vi"/>
				<Item Name="Get LV Class Default Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Default Value.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="Handle Open Word or Excel File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/NIReport.llb/Toolkit/Handle Open Word or Excel File.vi"/>
				<Item Name="imagedata.ctl" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/imagedata.ctl"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVCursorListTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVCursorListTypeDef.ctl"/>
				<Item Name="LVPositionTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVPositionTypeDef.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="NI_AALPro.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALPro.lvlib"/>
				<Item Name="NI_Excel.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Utility/NIReport.llb/Excel/NI_Excel.lvclass"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_Gmath.lvlib" Type="Library" URL="/&lt;vilib&gt;/gmath/NI_Gmath.lvlib"/>
				<Item Name="NI_HTML.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Utility/NIReport.llb/HTML/NI_HTML.lvclass"/>
				<Item Name="NI_LVConfig.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/config.llb/NI_LVConfig.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="NI_report.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Utility/NIReport.llb/NI_report.lvclass"/>
				<Item Name="NI_ReportGenerationCore.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/NIReport.llb/NI_ReportGenerationCore.lvlib"/>
				<Item Name="NI_ReportGenerationToolkit.lvlib" Type="Library" URL="/&lt;vilib&gt;/addons/_office/NI_ReportGenerationToolkit.lvlib"/>
				<Item Name="NI_Standard Report.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Utility/NIReport.llb/Standard Report/NI_Standard Report.lvclass"/>
				<Item Name="NI_Word.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Utility/NIReport.llb/Word/NI_Word.lvclass"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Number of Waveform Samples.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/Number of Waveform Samples.vi"/>
				<Item Name="OccFireType.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/OccFireType.ctl"/>
				<Item Name="Open Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Open Registry Key.vi"/>
				<Item Name="Open_Create_Replace File.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/Open_Create_Replace File.vi"/>
				<Item Name="ParseXMLFragments.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/ParseXMLFragments.vi"/>
				<Item Name="Path to URL.vi" Type="VI" URL="/&lt;vilib&gt;/printing/PathToURL.llb/Path to URL.vi"/>
				<Item Name="Read From XML File(array).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Read From XML File(array).vi"/>
				<Item Name="Read From XML File(string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Read From XML File(string).vi"/>
				<Item Name="Read From XML File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Read From XML File.vi"/>
				<Item Name="Read JPEG File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Read JPEG File.vi"/>
				<Item Name="Read PNG File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/png.llb/Read PNG File.vi"/>
				<Item Name="Read Registry Value DWORD.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value DWORD.vi"/>
				<Item Name="Read Registry Value Simple STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple STR.vi"/>
				<Item Name="Read Registry Value Simple U32.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple U32.vi"/>
				<Item Name="Read Registry Value Simple.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple.vi"/>
				<Item Name="Read Registry Value STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value STR.vi"/>
				<Item Name="Read Registry Value.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value.vi"/>
				<Item Name="Registry Handle Master.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Handle Master.vi"/>
				<Item Name="Registry refnum.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry refnum.ctl"/>
				<Item Name="Registry RtKey.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry RtKey.ctl"/>
				<Item Name="Registry SAM.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry SAM.ctl"/>
				<Item Name="Registry Simplify Data Type.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Simplify Data Type.vi"/>
				<Item Name="Registry View.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry View.ctl"/>
				<Item Name="Registry WinErr-LVErr.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry WinErr-LVErr.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Busy.vi"/>
				<Item Name="Set Cursor (Cursor ID).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Cursor ID).vi"/>
				<Item Name="Set Cursor (Icon Pict).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Icon Pict).vi"/>
				<Item Name="Set Cursor.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="STR_ASCII-Unicode.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/STR_ASCII-Unicode.vi"/>
				<Item Name="subFile Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/express/express input/FileDialogBlock.llb/subFile Dialog.vi"/>
				<Item Name="System Exec.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/system.llb/System Exec.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="Unset Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Unset Busy.vi"/>
				<Item Name="Wait On ActiveX Event.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/Wait On ActiveX Event.vi"/>
				<Item Name="Wait types.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/Wait types.ctl"/>
				<Item Name="Waveform Array To Dynamic.vi" Type="VI" URL="/&lt;vilib&gt;/express/express shared/transition.llb/Waveform Array To Dynamic.vi"/>
				<Item Name="WDT Number of Waveform Samples CDB.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples CDB.vi"/>
				<Item Name="WDT Number of Waveform Samples DBL.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples DBL.vi"/>
				<Item Name="WDT Number of Waveform Samples EXT.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples EXT.vi"/>
				<Item Name="WDT Number of Waveform Samples I8.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples I8.vi"/>
				<Item Name="WDT Number of Waveform Samples I16.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples I16.vi"/>
				<Item Name="WDT Number of Waveform Samples I32.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples I32.vi"/>
				<Item Name="WDT Number of Waveform Samples SGL.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples SGL.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Word Open method.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_office/word.llb/Word Open method.vi"/>
				<Item Name="Write BMP Data To Buffer.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Write BMP Data To Buffer.vi"/>
				<Item Name="Write BMP Data.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Write BMP Data.vi"/>
				<Item Name="Write BMP File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Write BMP File.vi"/>
				<Item Name="Write JPEG File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Write JPEG File.vi"/>
				<Item Name="Write PNG File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/png.llb/Write PNG File.vi"/>
				<Item Name="Write Registry Value DWORD.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Write Registry Value DWORD.vi"/>
				<Item Name="Write Registry Value Simple STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Write Registry Value Simple STR.vi"/>
				<Item Name="Write Registry Value Simple U32.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Write Registry Value Simple U32.vi"/>
				<Item Name="Write Registry Value Simple.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Write Registry Value Simple.vi"/>
				<Item Name="Write Registry Value STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Write Registry Value STR.vi"/>
				<Item Name="Write Registry Value.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Write Registry Value.vi"/>
				<Item Name="Write to XML File(array).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Write to XML File(array).vi"/>
				<Item Name="Write to XML File(string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Write to XML File(string).vi"/>
				<Item Name="Write to XML File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Write to XML File.vi"/>
			</Item>
			<Item Name="Advapi32.dll" Type="Document" URL="Advapi32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="Control button.ctl" Type="VI" URL="/C/Users/Admin/Desktop/Control button.ctl"/>
			<Item Name="Control string.ctl" Type="VI" URL="/C/Users/Admin/Desktop/Control string.ctl"/>
			<Item Name="kernel32.dll" Type="Document" URL="kernel32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="lvanlys.dll" Type="Document" URL="/&lt;resource&gt;/lvanlys.dll"/>
			<Item Name="System" Type="VI" URL="System">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="user32.dll" Type="Document" URL="user32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
